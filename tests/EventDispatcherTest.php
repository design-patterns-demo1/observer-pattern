<?php

namespace ElDama\Observer\Tests;

use ElDama\Observer\EventDispatcher;
use ElDama\Observer\Tests\Fixtures\FooListener;
use PHPUnit\Framework\TestCase;
use ElDama\Observer\EventListenerInterface;
use ElDama\Observer\EventInterface;
use ElDama\Observer\Tests\Fixtures\FooEvent;

class EventDispatcherTest extends TestCase
{
    public function testIfDispatchIsSuccessful(): void
    {
        $eventDispatcher = new EventDispatcher();
        $event = new FooEvent("io");
        $eventListener = new FooListener($event);

        $eventDispatcher->attach("foo", $eventListener);
        $this->assertEquals("io", $event->getTest());
        $eventDispatcher->dispatch("foo", $event);
        $this->assertEquals("HIT EM ALL BOI", $event->getTest());
    }
}