<?php

namespace ElDama\Observer\Tests\Fixtures;

use ElDama\Observer\EventListenerInterface;
use ElDama\Observer\Tests\Fixtures\FooEvent;
use ElDama\Observer\EventInterface;

class FooListener implements EventListenerInterface
{
    /**
     * @param ?FooEvent $event
     */
    public function listen(?EventInterface $event = null): void
    {
        $event->setTest("HIT EM ALL BOI");
    }
}