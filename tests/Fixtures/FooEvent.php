<?php

namespace ElDama\Observer\Tests\Fixtures;

use ElDama\Observer\EventInterface;

class FooEvent implements EventInterface
{
    private string $test;

    public function __construct(string $t)
    {
        $this->test = $t;
    }

    public function getTest(): string
    {
        return $this->test;
    }

    /**
     * @param string $value
     */
    public function setTest(string $value): self
    {
        $this->test = $value;
        
        return $this;
    }
}