<?php

namespace ElDama\Observer;

use ElDama\Observer\EventDispatcherInterface;
use ElDama\Observer\EventListenerInterface;
use ElDama\Observer\EventInterface;

class EventDispatcher implements EventDispatcherInterface
{

    /**
     * @var array<string, array<int, EventListenerInterface>>
     */
    private array $listeners = [];

    public function dispatch(string $eventName, ?EventInterface $event = null): void
    {
        foreach($this->listeners[$eventName] as $listener) {
            $listener->listen($event);
        }
    }

    public function attach(string $eventName, EventListenerInterface $listener): void
    {
        $this->listeners[$eventName][] = $listener;
    }
}