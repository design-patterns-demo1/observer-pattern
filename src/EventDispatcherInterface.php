<?php

namespace ElDama\Observer;

use ElDama\Observer\EventListenerInterface;
use ElDama\Observer\EventInterface;

interface EventDispatcherInterface
{
    public function dispatch(string $eventName, ?EventInterface $event = null): void;

    public function attach(string $eventName, EventListenerInterface $listener): void;
}