<?php

namespace ElDama\Observer;

use ElDama\Observer\EventInterface;
interface EventListenerInterface
{
    public function listen(?EventInterface $event): void;
}